@extends('layouts.app')

@section('content')

    <div class="text-center">
        <div>
            <img src="https://1485993846.rsc.cdn77.org/wp-content/uploads/2022/05/laravel.png" class="w-50">
        </div>

        <div class="mx-auto text-center">
            
            <h2>Featured Posts</h2>

            @if(count($posts) > 0)
                @foreach($posts as $post)
                    <div class="card text-center">
                        <div class="card-body">
                            <h4 class="card-title mb-3">
                                <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                            </h4>
                            <h5 class="card-text mb-3">Author: {{$post->user->name}}</h5>
                        </div>
                    </div>
                @endforeach
            @else
                    <h2>There are no posts to show</h2>
            @endif
        </div>

@endsection

